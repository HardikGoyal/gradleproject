#!/bin/sh  

mkdir -p build/diffForFiles
  
file='build/diffFiles.txt'  
  
i=1  
while read line; do 
  mkdir -p "$(dirname "build/diffForFiles/$line")"
  git diff $CI_MERGE_REQUEST_DIFF_BASE_SHA $CI_COMMIT_SHA -- $line > "build/diffForFiles/$line"
done < $file  
