#!/bin/bash

diffList=''
diffCompiledClasses=''

readDiffFile () {
  diffFile=build/diffFiles.txt

  while read -r file; do
      
    if [[ $file =~ src/main/java/.*java ]]; then
      
      fileName=${file#src/main/java/}

      diffList+=\"$fileName\"
      diffList+=,

      fileNameWithoutExtension=${fileName%.java}
      
      diffCompiledClasses+=\"$fileNameWithoutExtension\"
      diffCompiledClasses+=,
    fi

#      if [[ $file =~ src/test/java/.*java ]]; then

#       fileName=${file#src/test/java/}

#       diffList+=\"$fileName\"
#       diffList+=,

#       fileNameWithoutExtension=${fileName%.java}
      
#       diffCompiledClasses+=\"$fileNameWithoutExtension\"
#       diffCompiledClasses+=,
#     fi
  done <$diffFile

  diffList=${diffList%,}
  diffCompiledClasses=${diffCompiledClasses%,}       
}
      
readDiffFile

check="./gradlew checkstyleMain -PcheckstyleIncludes=`echo $diffList`"
eval $check
spot="./gradlew spotbugsMain -PspotbugsOnlyAnalyze=`echo $diffCompiledClasses`"
eval $spot
